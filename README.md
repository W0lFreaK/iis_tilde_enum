# iis_tilde_enum
Нужно создать плагин для Burp Suite, который бы добавлял в актив скан доп проверки на IIS Tilde Enum.
Полезные ссылки:
* Burp:
	+ [Сэмпл плагина](https://github.com/PortSwigger/example-scanner-checks)
	+ [Плагин с новыми проверками сканера 1](https://github.com/PortSwigger/active-scan-plus-plus)
	+ [Плагин с новыми проверками сканера 2](https://github.com/PortSwigger/additional-scanner-checks)
	+ [И еще один пример плагина с реквестами](https://github.com/doyensec/burpdeveltraining/blob/master/DetectELJ/Python/Final/BurpExtender.py)
	+ [Разработка расширения с GUI](https://parsiya.net/blog/2019-11-04-swing-in-python-burp-extensions-part-1/)
* IIS Tilde Enum:
	+ [Описание баги](https://soroush.secproject.com/downloadable/microsoft_iis_tilde_character_vulnerability_feature.pdf)
	+ [Пример реализации сканера](https://github.com/WebBreacher/tilde_enum)

Проект разрабатывался в рамках [Summ3r 0f h4ck 2020](https://dsec.ru/about/summerofhack/)